FROM openjdk:8-jdk-alpine
VOLUME [ "/tmp" ]
ARG JAVA_OPTS
ENV JAVA_OPTS=${JAVA_OPTS}
ADD target/*.jar id08-crud-persona.jar
EXPOSE 8888
ENTRYPOINT exec java ${JAVA_OPTS} -jar id08-crud-persona.jar
