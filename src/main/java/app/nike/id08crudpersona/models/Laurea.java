package app.nike.id08crudpersona.models;

import lombok.Data;

@Data
public class Laurea {
	
	private long id;

	private String laurea;

	private String annoConseguimento;

}
