package app.nike.id08crudpersona.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Document (collection = "persona")
public class Persona {
	@Id
	@JsonIgnore
	private ObjectId id;

	@NotNull
	private String codiceFiscale;
	
	private String nome;
	
	private String cognome;
	
	private String email;
	
	private String dataNascita;
	
	private String luogoNascita;
	
	private String cittadinanza;
	
	private String residenza;
	
	private String capResidenza;
	
	private String domicilio;
	
	private String capDomicilio;
	
	private String recapitoMobile;
	
	private String recapitoFisso;
	
	private boolean categoriaProtetta;
	
	private boolean candanneCivili;
	
	private String canale;
	
	private Diploma diploma;
	
	private List<Laurea> laurea;
	
	private List<Lavoro> lavoro;
	
	private List<Certificazione> certificazione;
	
	private boolean privacy=false;

	
}
