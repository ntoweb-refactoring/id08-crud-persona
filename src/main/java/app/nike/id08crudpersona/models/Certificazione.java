package app.nike.id08crudpersona.models;

import lombok.Data;

@Data
public class Certificazione {

	private long id;

	private String nomeCertificazione;
	
	private String scadenzaCertificazione;
	
	private Persona persona;
	
}
