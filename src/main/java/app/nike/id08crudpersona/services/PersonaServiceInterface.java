package app.nike.id08crudpersona.services;

import app.nike.id08crudpersona.models.Persona;

import java.util.List;

public interface PersonaServiceInterface {

    public Persona addPersona(Persona p) throws Exception;
    public Persona updatePersona(Persona p) throws Exception;
    public void deletePersona(String codiceFiscale) throws Exception;
    public Persona getPersona(String codiceFiscale) throws Exception;
    public List<Persona> getPersone() throws Exception;
    }
