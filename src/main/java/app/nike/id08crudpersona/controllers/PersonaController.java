package app.nike.id08crudpersona.controllers;

import app.nike.id08crudpersona.models.Persona;
import app.nike.id08crudpersona.services.PersonaServiceInterface;
import com.netflix.client.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/nike/persona")
public class PersonaController {

    @Autowired
    private PersonaServiceInterface personaService;

    /**
     * 
     * @param p
     * @return
     * @throws Exception
     */
    @PostMapping("/salva")
    public ResponseEntity<Persona> addPersona (@RequestBody Persona p) throws Exception {
        long intime = System.currentTimeMillis();
        try {
            return new ResponseEntity<Persona>(personaService.addPersona(p), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("messagio di errore----> " + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            System.out.println("tempo di esecuzione--------->" + ((System.currentTimeMillis()) - intime));
        }
    }

    /**
     * 
     * @param p
     * @return
     */
    @PostMapping("/aggiorna")
    public Persona updatePersona(@RequestBody Persona p) {
        try {
            return personaService.updatePersona(p);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /**
     * 
     * @param codiceFiscale
     * @throws Exception
     */
    @DeleteMapping("/cancella/{codiceFiscale}")
    public void deletePersona(@PathVariable(value = "codiceFiscale") String codiceFiscale) throws Exception {

        try {
            personaService.deletePersona(codiceFiscale);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    /**
     * 
     * @param id
     * @param htt
     * @return
     */
    @GetMapping("/recupera/{codiceFiscale}")
    public Persona getPersona(@PathVariable(value = "codiceFiscale") String id, HttpHeaders htt) {

        try {
            return personaService.getPersona(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /**
     * 
     * @return
     */
    @GetMapping("/recupera")
    public List<Persona> getPersone() {

        try {
            return personaService.getPersone();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
